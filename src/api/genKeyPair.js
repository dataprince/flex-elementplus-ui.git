import request from '@/utils/request'
export function genKeyPair(callback) {
  return request({
    url: '/genKeyPair',
    headers: {
      isToken: false,
      repeatSubmit: false
    },
    method: 'get'
  }).then((res) => {
    return callback(res.data.uuidPrivateKey, res.data.RSA_PUBLIC_KEY)
  })

}
