import request from '@/utils/request'

// 查询客户主表(mb)列表
export function listCustomer(query) {
  return request({
    url: '/demo/customer/list',
    method: 'get',
    params: query
  })
}

// 查询客户主表(mb)详细
export function getCustomer(customerId) {
  return request({
    url: '/demo/customer/' + customerId,
    method: 'get'
  })
}

// 新增客户主表(mb)
export function addCustomer(data) {
  return request({
    url: '/demo/customer',
    method: 'post',
    data: data
  })
}

// 修改客户主表(mb)
export function updateCustomer(data) {
  return request({
    url: '/demo/customer',
    method: 'put',
    data: data
  })
}

// 删除客户主表(mb)
export function delCustomer(customerId) {
  return request({
    url: '/demo/customer/' + customerId,
    method: 'delete'
  })
}
